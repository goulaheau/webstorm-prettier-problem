# Webstorm Prettier problem

Repository to showcase a problem with the checkbox `On "Reformat Code" action` enabled in Prettier settings.

## Setup

Run in terminal :

```
npm install
```

## Explanation

By running `Reformat Code` action on index.html, this is the result : 

```html
<div>
  <div></div>
</div>

```

Whereas running Prettier returns this :

```html
<div><div></div></div>

```
